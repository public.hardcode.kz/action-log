using System;
using System.Linq;
using System.Linq.Expressions;
using ActionLog.Module.Models;
using Microsoft.EntityFrameworkCore;

namespace ActionLog.Module.Repos
{
    public class ActionLogExtraDataRepo<TContext> : IActionLogExtraDataRepo where TContext : DbContext
    {
        private readonly DbSet<ActionLogExtraData> _repo;

        public ActionLogExtraDataRepo(TContext context)
        {
            _repo = context.Set<ActionLogExtraData>();
        }
        
        public IQueryable<ActionLogExtraData> SelectQuery(Expression<Func<ActionLogExtraData, bool>> expression)
        {
            return _repo.Where(expression);
        }
    }
}