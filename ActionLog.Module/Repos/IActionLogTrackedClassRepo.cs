using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ActionLog.Module.Models;
using Microsoft.EntityFrameworkCore;

namespace ActionLog.Module.Repos
{
    public interface IActionLogTrackedClassRepo<TContext> where TContext : DbContext
    {
        Task<bool> IsTracked(string classFullName);

        Task<IEnumerable<ActionLogTrackedClass>> Get();

        /// <summary>
        /// Фильтрация и получение IQueryable
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        IQueryable<ActionLogTrackedClass> SelectQuery(Expression<Func<ActionLogTrackedClass, bool>> expression);

        /// <summary>
        /// Обновление записи в таблице
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task UpdateAsync(ActionLogTrackedClass entity);

        /// <summary>
        /// Добавление записи в таблицу
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task AddAsync(ActionLogTrackedClass entity);
    }
}