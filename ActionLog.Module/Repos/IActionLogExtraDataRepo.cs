using System;
using System.Linq;
using System.Linq.Expressions;
using ActionLog.Module.Models;

namespace ActionLog.Module.Repos
{
    public interface IActionLogExtraDataRepo
    {
        IQueryable<ActionLogExtraData> SelectQuery(Expression<Func<ActionLogExtraData, bool>> expression);
    }
}