using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ActionLog.Module.Repos
{
    public interface IActionLogRepo
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        Task<object> Get();

        Task<IEnumerable<Module.Models.ActionLog>> Select(Expression<Func<Module.Models.ActionLog, bool>> expression);

        /// <summary>
        /// Select query
        /// </summary>
        /// <returns></returns>
        IQueryable<Module.Models.ActionLog> SelectQuery(Expression<Func<Module.Models.ActionLog, bool>> expression);

        /// <summary>
        /// Select with pagination
        /// </summary>
        /// <returns></returns>
        (Task<int>, IQueryable<Module.Models.ActionLog>) SelectQueryPagination(
            Expression<Func<Module.Models.ActionLog, bool>> expression,
            int start = 1, int rowCount = 100);
    }
}