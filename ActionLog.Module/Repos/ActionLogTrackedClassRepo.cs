using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ActionLog.Module.Models;
using Microsoft.EntityFrameworkCore;

namespace ActionLog.Module.Repos
{
    public class ActionLogTrackedClassRepo<TContext> : IActionLogTrackedClassRepo<TContext>
        where TContext : DbContext
    {
        private readonly DbSet<ActionLogTrackedClass> _repo;
        private readonly TContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ActionLogTrackedClassRepo{TContext}"/> class.
        /// </summary>
        /// <param name="context">DbContext.</param>
        public ActionLogTrackedClassRepo(TContext context)
        {
            _context = context;
            _repo = context.Set<ActionLogTrackedClass>();
        }

        /// <inheritdoc/>
        public async Task AddAsync(ActionLogTrackedClass entity)
        {
            await _repo.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task UpdateAsync(ActionLogTrackedClass entity)
        {
            _repo.Update(entity);
            await _context.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task<bool> IsTracked(string classFullName)
        {
            return await _repo.AnyAsync(x => x.ClassFullName == classFullName && x.IsTracked);
        }

        /// <inheritdoc/>
        public IQueryable<ActionLogTrackedClass> SelectQuery(Expression<Func<ActionLogTrackedClass, bool>> expression)
        {
            return _repo.Where(expression);
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<ActionLogTrackedClass>> Get()
        {
            return await _repo.AsNoTracking()
                .Where(x => true).ToListAsync();
        }
    }
}