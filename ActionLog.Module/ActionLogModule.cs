using System;
using ActionLog.Module.Dtos;
using ActionLog.Module.Logics;
using ActionLog.Module.Logics.Impl;
using ActionLog.Module.Repos;
using HardCode.ICacheService;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace ActionLog.Module
{
    /// <summary>
    /// Module for actionLog
    /// </summary>
    public static class ActionLogModule
    {
        /// <summary>
        /// Add to service all files
        /// </summary>
        /// <param name="services">IServiceCollection.</param>
        /// <typeparam name="TContext">DbContext.</typeparam>
        /// <typeparam name="TUser">User class.</typeparam>
        /// <typeparam name="TKey">User class ID type.</typeparam>
        /// <typeparam name="TOutActionGroupLogDto">Return dto.</typeparam>
        /// <typeparam name="TCacheService">ICacheService.</typeparam>
        public static void ConfigureActionLog
            <TContext, TUser, TKey, TOutActionGroupLogDto, TCacheService>(this IServiceCollection services)
            where TContext : DbContext
            where TKey : IEquatable<TKey>
            where TUser : IdentityUser<TKey>
            where TOutActionGroupLogDto : ActionGroupLogDto, new()
            where TCacheService : ICacheService
        {
            services.AddScoped<IActionLogExtraDataRepo, ActionLogExtraDataRepo<TContext>>();
            services.AddScoped<IActionLogRepo, ActionLogRepo<TContext>>();
            services.AddScoped<IActionLogTrackedClassRepo<TContext>, ActionLogTrackedClassRepo<TContext>>();
            services.AddScoped<IActionLogViewLogic
                <TOutActionGroupLogDto, TUser, TKey>,
                ActionLogViewLogic<TContext, TUser, TKey, TOutActionGroupLogDto, TCacheService>>();
            // services.AddTransient<IActionLogExtraDataLogic,
            //     ActionLogViewLogic<TContext, TUser, TKey, TOutActionGroupLogDto, TCacheService>>();
            services.AddScoped<IActionLogSettingLogic, ActionLogSettingLogic<TContext>>();
            services.AddScoped<IActionLogExtraDataLogic, ActionLogHttpContextLogic>();
            services.AddScoped<IActionLogHttpContextLogic, ActionLogHttpContextLogic>();
        }
    }
}