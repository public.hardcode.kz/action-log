using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ActionLog.Module.Dtos;
using ActionLog.Module.Logics;
using HardCode.ICacheService;
using Microsoft.EntityFrameworkCore;

namespace ActionLog.Module.Resolvers
{
    public abstract class ActionLogBaseResolver<TContext, TEntity> : IActionLogResolver
        where TContext : DbContext 
        where TEntity : class
    {
        private readonly TContext _dbContext;
        private readonly ICacheService _cacheService;

        public ActionLogBaseResolver(
            TContext context, 
            ICacheService cacheService)
        {
            _dbContext = context;
            _cacheService = cacheService;
        }
        protected abstract Expression<Func<TEntity, ActionLogResolverTableKeyValueDto>> SelectExpression();

        public async Task<string> ResolveTitleAsync(string objectId)
        {
            var values =  await GetTableValues(SelectExpression());
            return values.FirstOrDefault(x => x.Key == objectId)?.Value;
        }

        public string ResolveTitle(string objectId)
        {
            var wait = ResolveTitleAsync(objectId);
            wait.Wait();
            return wait.Result;
        }

        private async Task<IEnumerable<ActionLogResolverTableKeyValueDto>> GetTableValues
            (Expression<Func<TEntity, ActionLogResolverTableKeyValueDto>> select)
        {
            var key = "resolverTableValues" + GetType();
            var result = _cacheService.Get<IEnumerable<ActionLogResolverTableKeyValueDto>>(key);
            if (result != null)
            {
                return result;
            }
            
            result = await _dbContext.Set<TEntity>()
                .AsNoTracking()
                .Select(select)
                .ToListAsync();
            
            _cacheService.Store(key, result, 60);
            return result;
        }
    }
}