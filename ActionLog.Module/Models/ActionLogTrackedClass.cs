using System.ComponentModel.DataAnnotations;

namespace ActionLog.Module.Models
{
    /// <summary>
    /// Отслеживаемые сущности
    /// </summary>
    public class ActionLogTrackedClass
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Имя класса
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// Полное имя класса
        /// </summary>
        public string ClassFullName { get; set; }

        /// <summary>
        /// Квалифицированное имя сборки
        /// </summary>
        public string AssemblyQualifiedName { get; set; }

        /// <summary>
        /// Отслеживаются ли изменения в данной сущности
        /// </summary>
        public bool IsTracked { get; set; }
    }
}