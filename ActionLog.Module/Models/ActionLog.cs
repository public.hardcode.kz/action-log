using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ActionLog.Module.Primitives;
using Microsoft.EntityFrameworkCore;

namespace ActionLog.Module.Models
{
    /// <summary>
    /// Журнал действий
    /// </summary>
    public class ActionLog
    {
        [Key]
        public int Id { get; set; }
        
        /// <summary>
        /// Группировка записей
        /// </summary>
        public Guid GroupGuid { get; set; }
        
        /// <summary>
        /// ID пользователя
        /// </summary>
        public string UserId { get; set; }
        
        /// <summary>
        /// Сущность, в которой произошли изменения
        /// </summary>
        public string ObjectName { get; set; }
        
        /// <summary>
        /// Полное название сущности
        /// </summary>
        public string ObjectFullName { get; set; }

        /// <summary>
        /// Колонка, в которой произошли изменения
        /// </summary>
        public string Column { get; set; }
        
        /// <summary>
        /// ID сущности
        /// </summary>
        public string ObjectId { get; set; }
        
        /// <summary>
        /// Старое значение
        /// </summary>
        public string OldValue { get; set; }
        
        /// <summary>
        /// Новое значение
        /// </summary>
        public string NewValue { get; set; }
        
        /// <summary>
        /// Тип операции
        /// </summary>
        public EntityState State { get; set; }
        
        /// <summary>
        /// Дата создания сущности
        /// </summary>
        public DateTime DateCreate { get; set; } = DateTime.Now;

        /// <summary>
        /// Тип действия 
        /// </summary>
        public string ActionLogTypeEnum { get; set; } = "Default";
        
        /// <summary>
        /// Группировка по действию
        /// </summary>
        public Guid? ActionLogTypeGroupId {get;set;}

    }
}