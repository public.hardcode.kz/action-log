using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace ActionLog.Module.Dtos
{
    public class ActionLogQuery
    {
        /// <summary>
        /// Дата начала
        /// </summary>
        public DateTime? StartDate { get; set; }
        
        /// <summary>
        /// Дата окончания
        /// </summary>
        public DateTime? EndDate { get; set; }
        
        /// <summary>
        /// ID пользователя
        /// </summary>
        public string UserId { get; set; }
        
        /// <summary>
        /// Сущность, в которой произошли изменения (Event)
        /// </summary>
        public string Object { get; set; }

        /// <summary>
        /// Сущность, в которой произошли изменения (DMMS.Core.Models.Event)
        /// </summary>
        public string ObjectFullName { get; set; }

        /// <summary>
        /// Колонка, в которой произошли изменения
        /// </summary>
        public string Column { get; set; }

        /// <summary>
        /// ID сущности
        /// </summary>
        public string ObjectId { get; set; }

        /// <summary>
        /// Старое значение
        /// </summary>
        public string OldValue { get; set; }

        /// <summary>
        /// Новое значение
        /// </summary>
        public string NewValue { get; set; }

        /// <summary>
        /// Тип операции
        /// </summary>
        public EntityState? State { get; set; }

        public List<string> ActionLogTypes { get; set; }

        /// <summary>
        /// Номер страницы
        /// </summary>
        public int Page { get; set; } = 1;

        /// <summary>
        /// Кол-во строк на странице
        /// </summary>
        public int RowCount { get; set; } = 100;
    }
}