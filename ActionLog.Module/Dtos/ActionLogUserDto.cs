namespace ActionLog.Module.Dtos
{
    public class ActionLogUserDto<T>
    {
        public T Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}