using System.Text.Json.Serialization;
using ActionLog.Module.Logics;

namespace ActionLog.Module.Dtos
{
    public class ActionLogClassAttributeInfoExtraDto : ActionLogClassAttributeInfoDto
    {
        [JsonIgnore]
        public IActionLogResolver Resolver { get; set; }
    }
}