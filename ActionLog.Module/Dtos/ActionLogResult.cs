namespace ActionLog.Module.Dtos
{
    public class ActionLogResult<T, TOutActionGroupLogDto>
    {
        public T Item { get; set; }

        public ActionLogSearhResultDto<TOutActionGroupLogDto> LogResult { get; set; }
    }
}