using System.Collections.Generic;
using System.Text.Json.Serialization;
using ActionLog.Module.Logics;
using ActionLog.Module.Primitives;

namespace ActionLog.Module.Dtos
{
    public class ActionLogClassAttributeInfoDto
    {
        /// <summary>
        /// Наименование
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// Наименование для отображения
        /// </summary>
        public string ColumnDisplayName { get; set; }

        public ActionLogSettingItemType ColumnType { get; set; }

        public string ColumnTypeName => ColumnType.ToString().ToLower();

        public IEnumerable<ActionLogBaseEnumValuesDto> EnumValues { get; set; }
    }
}