using System.Collections.Generic;

namespace ActionLog.Module.Dtos.Grouped
{
    public class ActionLogGroupTypeEntityDto
    {
        public string ObjectFullName { get; set; }
        
        public string ObjectDescription { get; set; }

        public IEnumerable<ActionLogGroupTypeEntityObjectDto> Objects { get; set; }
    }
}