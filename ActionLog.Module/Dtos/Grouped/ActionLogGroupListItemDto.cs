using System;
using System.Collections.Generic;

namespace ActionLog.Module.Dtos.Grouped
{
    public class ActionLogGroupListItemDto
    {
        /// <summary>
        /// Gets or sets userId
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets userName
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets userEmail
        /// </summary>
        public string UserEmail { get; set; }
        
        /// <summary>
        /// Gets or sets actionId
        /// </summary>
        public Guid? ActionId { get; set; }
        
        /// <summary>
        /// Gets or sets action
        /// </summary>
        public string Action { get; set; }
        
        /// <summary>
        /// Gets or sets modificationDate
        /// </summary>
        public DateTime ModificationDate { get; set; }
        
        /// <summary>
        /// Gets modification count
        /// </summary>
        public int ModificationCount { get; set; }
        
        /// <summary>
        /// Gets or sets modification tables
        /// </summary>
        public IEnumerable<string> Items { get; set; }
    }
}