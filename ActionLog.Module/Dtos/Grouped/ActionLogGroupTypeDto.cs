using System;
using System.Collections.Generic;

namespace ActionLog.Module.Dtos.Grouped
{
    public class ActionLogGroupTypeDto
    {
        public string UserId { get; set; }
        
        public string UserName { get; set; }
        
        public string UserEmail { get; set; }

        public DateTime? ModificationDate { get; set; }
        public Guid? ActionId { get; set; }

        public string Action { get; set; }

        public string ExtraData { get; set; }

        public IEnumerable<ActionLogGroupTypeEntityDto> Entities { get; set; }
    }
}