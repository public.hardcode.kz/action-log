using System.Collections.Generic;

namespace ActionLog.Module.Dtos.Grouped
{
    public class ActionLogGroupTypeEntityObjectDto
    {
        public string ObjectId { get; set; }

        public IEnumerable<ActionLogDto> Modifications { get; set; }
    }
}