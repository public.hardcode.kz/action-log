using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ActionLog.Module.Dtos.Grouped
{
    public class ActionLogGroupListQueryDto
    {
        /// <summary>
        /// Дата начала
        /// </summary>
        public DateTime? StartDate { get; set; }
        
        /// <summary>
        /// Дата окончания
        /// </summary>
        public DateTime? EndDate { get; set; }
        
        /// <summary>
        /// ID пользователя
        /// </summary>
        public object UserId { get; set; }
        
        /// <summary>
        /// Сущность, в которой произошли изменения (DMMS.Core.Models.Event)
        /// </summary>
        public string ObjectFullName { get; set; }
        
        public List<string> ActionLogTypes { get; set; }
        
        /// <summary>
        /// Номер страницы
        /// </summary>
        public int Page { get; set; } = 1;

        /// <summary>
        /// Кол-во строк на странице
        /// </summary>
        public int RowCount { get; set; } = 100;
    }
}