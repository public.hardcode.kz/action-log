namespace ActionLog.Module.Dtos
{
    public class ActionLogBaseEnumValuesDto
    {
        /// <summary>
        /// Gets or sets value
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets stringValue
        /// </summary>
        public string StringValue { get; set; }

        /// <summary>
        /// Gets or sets description
        /// </summary>
        public string Description { get; set; }
    }
}