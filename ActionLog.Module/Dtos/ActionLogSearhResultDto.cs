using System.Collections.Generic;

namespace ActionLog.Module.Dtos
{
    public class ActionLogSearhResultDto<TOutActionGroupLogDto>
    {
        public int Total { get; set; }

        public int CurrentPage { get; set; }

        public int RowCount { get; set; }

        public IEnumerable<TOutActionGroupLogDto> Items { get; set; } = new List<TOutActionGroupLogDto>();
    }
}