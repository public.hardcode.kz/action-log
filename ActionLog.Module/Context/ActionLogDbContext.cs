using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ActionLog.Module.Dtos;
using ActionLog.Module.Logics.Impl;
using ActionLog.Module.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace ActionLog.Module.Context
{
    /// <summary>
    /// DbCOntext
    /// </summary>
    /// <typeparam name="TDbContext">DbContext</typeparam>
    public abstract class ActionLogDbContext<TDbContext> : DbContext, IActionLogContext<TDbContext>
        where TDbContext : DbContext
    {
        protected List<ActionLogTrackedClass> TrackedClasses;
        /// <summary>
        /// Initializes a new instance of the <see cref="ActionLogDbContext{TDbContext}"/> class.
        /// </summary>
        /// <param name="options">DbContextOptions.</param>
        public ActionLogDbContext(DbContextOptions options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets журнал изменений в таблицах
        /// </summary>
        public DbSet<Models.ActionLog> ActionLogs { get; set; }

        /// <summary>
        /// Gets or sets доступные для логирования таблицы
        /// </summary>
        public DbSet<ActionLogTrackedClass> ActionLogTrackeds { get; set; }
        
        /// <summary>
        /// Таблица с дополнительными данными о действии
        /// </summary>
        public DbSet<ActionLogExtraData> ActionLogExtraDatas { get; set; }


        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Models.ActionLog>()
                .HasIndex(b => b.ObjectId);
            modelBuilder.Entity<Models.ActionLog>()
                .HasIndex(b => b.ActionLogTypeGroupId);
            modelBuilder.Entity<ActionLogExtraData>()
                .HasIndex(b => b.GroupGuid);
        }

        /// <summary>
        /// Асинхронное сохранение в БД без записи в журнал действий
        /// </summary>
        /// <returns>Id.</returns>
        public async Task<int> BaseSaveChangesAsync()
        {
            return await base.SaveChangesAsync();
        }

        /// <summary>
        /// Сохранение в БД без записи в журнал действий
        /// </summary>
        /// <returns>Int.</returns>
        public int BaseSaveChanges()
        {
            return base.SaveChanges();
        }


        /// <summary>
        /// Сохранение в БД с записью в журнал действий
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            var res = DetectChange(cancellationToken, "async");

            return res.TaskIntValue;
        }

        /// <summary>
        /// Сохранение в БД с записью в журнал действий
        /// </summary>
        /// <returns></returns>
        public override int SaveChanges()
        {
            var res = DetectChange();

            return res.IntValue;
        }

        /// <summary>
        /// Обнаружение изменений
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<int> DetectChangeSaveAsync(CancellationToken cancellationToken)
        {
            var objId = base.SaveChangesAsync(cancellationToken);
            objId.Wait(cancellationToken);
            return objId;
        }

        /// <summary>
        /// Получение сущностей, которые подлежат логированию
        /// </summary>
        /// <returns></returns>
        public virtual List<ActionLogTrackedClass> GetTrackedEntities()
        {
            if (TrackedClasses != null && TrackedClasses.Any())
            {
                return TrackedClasses;
            }
            
            var query = ActionLogTrackeds.Where(x => x.IsTracked).AsNoTracking().ToListAsync();
            query.Wait();
            TrackedClasses =  query.Result;
            return TrackedClasses;
        }

        public abstract ActionLogBaseLogic<TDbContext> CreateActionLogBaseLogic();

        /// <summary>
        /// Получение изменений в сущности и сохранение результата
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <param name="saveType"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public SaveType DetectChange(CancellationToken cancellationToken = new CancellationToken(),
            string saveType = "sync")
        {
            var result = new SaveType();
            ChangeTracker.DetectChanges();
            var actionLog = CreateActionLogBaseLogic();
            
            var trackedResult = GetTrackedEntities();
            var modifiedEntities = ChangeTracker.Entries()
                .Where(p => p.State == EntityState.Modified || p.State == EntityState.Added ||
                            p.State == EntityState.Deleted || p.State == EntityState.Detached)
                .ToList();
            var groupGuid = Guid.NewGuid();
            var hasChange = false;
            var newObjects = new List<EntityEntry>();
            foreach (var change in modifiedEntities)
            {
                var type = change.Entity.GetType();
                var isTracked =
                    trackedResult.Any(x => x.ClassFullName == type.FullName);

                if (!isTracked)
                {
                    continue;
                }
                
                switch (change.State)
                {
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        break;

                    case EntityState.Deleted:
                        actionLog.DetectContextChanges(change, groupGuid);
                        hasChange = true;
                        break;

                    case EntityState.Modified:
                        actionLog.DetectContextChanges(change, groupGuid);
                        hasChange = true;
                        break;

                    case EntityState.Added:
                        newObjects.Add(change);
                        hasChange = true;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            if (hasChange)
            {
                actionLog.SaveExtraData();
            }

            if (newObjects.Count > 0)
            {
                if (saveType == "sync") result.IntValue = base.SaveChanges();
                else result.TaskIntValue = DetectChangeSaveAsync(cancellationToken);

                foreach (var entityEntry in newObjects)
                {
                    actionLog.NewObject(entityEntry, groupGuid);
                }

                if (saveType == "sync") result.IntValue = base.SaveChanges();
                else result.TaskIntValue = DetectChangeSaveAsync(cancellationToken);
            }


            if (saveType == "sync")
            {
                if (result.IntValue != 0) return result;
            }
            else
            {
                if (result.TaskIntValue != null) return result;
            }
            
            if (saveType == "sync") result.IntValue = base.SaveChanges();
            else result.TaskIntValue = DetectChangeSaveAsync(cancellationToken);
            return result;

        }
    }
}