using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ActionLog.Module.Dtos;
using ActionLog.Module.Logics;
using ActionLog.Module.Logics.Impl;
using ActionLog.Module.Models;
using Microsoft.EntityFrameworkCore;

namespace ActionLog.Module.Context
{
    /// <summary>
    /// Interface for actionLogDbContext.
    /// </summary>
    /// <typeparam name="TDbContext">DbContext.</typeparam>
    public interface IActionLogContext<TDbContext>
        where TDbContext : DbContext
    {
        DbSet<Models.ActionLog> ActionLogs { get; set; }

        DbSet<ActionLogTrackedClass> ActionLogTrackeds { get; set; }

        /// <summary>
        /// Асинхронное сохранение в БД без записи в журнал действий
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        Task<int> BaseSaveChangesAsync();

        /// <summary>
        /// Сохранение в БД без записи в журнал действий
        /// </summary>
        /// <returns>int.</returns>
        int BaseSaveChanges();

        /// <summary>
        /// Обнаружение изменений
        /// </summary>
        /// <param name="cancellationToken">CancellationToken.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        Task<int> DetectChangeSaveAsync(CancellationToken cancellationToken);

        /// <summary>
        /// Получение сущностей, которые подлежат логированию
        /// </summary>
        /// <returns>Return ActionLogTrackedClass.</returns>
        List<ActionLogTrackedClass> GetTrackedEntities();

        /// <summary>
        /// Получение изменений в сущности и сохранение результата
        /// </summary>
        /// <param name="cancellationToken">cancellationToken</param>
        /// <param name="saveType">SaveType (sync, async).</param>
        /// <returns>SaveType.</returns>
        SaveType DetectChange(
            CancellationToken cancellationToken = default,
            string saveType = "sync");

        ActionLogBaseLogic<TDbContext> CreateActionLogBaseLogic();
    }
}