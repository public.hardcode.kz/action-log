namespace ActionLog.Module.Logics
{
    public interface IActionLogExtraDataLogic
    {
        /// <summary>
        /// Сохранение расширенных данных
        /// </summary>
        /// <param name="data"></param>
        void SetExtraData(object data);
    }
}