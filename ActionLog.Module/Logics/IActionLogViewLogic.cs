using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ActionLog.Module.Dtos;
using ActionLog.Module.Dtos.Grouped;

namespace ActionLog.Module.Logics
{
    public interface IActionLogViewLogic<TOutActionGroupLogDto, TUser, TKey>
    {
        /// <summary>
        /// Get.
        /// </summary>
        /// <param name="filter">Filter for query.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        Task<ActionLogSearhResultDto<TOutActionGroupLogDto>> Get(ActionLogQuery filter);

        /// <summary>
        /// Get logs. Additional user field select
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="userQuery"></param>
        /// <returns></returns>
        Task<ActionLogSearhResultDto<TOutActionGroupLogDto>> Get(ActionLogQuery filter,
            Expression<Func<TUser, ActionLogUserDto<TKey>>> userQuery);

        Task<ActionLogSearhResultDto<TOutActionGroupLogDto>> Get(ActionLogQuery filter,
            List<ActionLogUserDto<TKey>> users);

        #region GetGrouped

        Task<ActionLogGroupTypeDto> GetGrouped(Guid actionLogTypeId);

        Task<ActionLogGroupTypeDto> GetGrouped(Guid actionLogTypeId,
            Expression<Func<TUser, ActionLogUserDto<TKey>>> userQuery);

        Task<T> GetGrouped<T>(Guid actionLogTypeId) where T : ActionLogGroupTypeDto, new();

        Task<T> GetGrouped<T>(Guid actionLogTypeId, Expression<Func<TUser, ActionLogUserDto<TKey>>> userQuery)
            where T : ActionLogGroupTypeDto, new();

        Task<ActionLogGroupTypeDto> GetGrouped(Guid actionLogTypeId, List<ActionLogUserDto<TKey>> users);

        Task<T> GetGrouped<T>(Guid actionLogTypeId, List<ActionLogUserDto<TKey>> users)
            where T : ActionLogGroupTypeDto, new();

        #endregion

        /// <summary>
        /// Получение списка изменений с группировкой по действию
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="userQuery"></param>
        /// <returns></returns>
        Task<ActionLogSearhResultDto<T>> GroupList<T>(ActionLogGroupListQueryDto filter,
            Expression<Func<TUser, ActionLogUserDto<TKey>>> userQuery) where T : ActionLogGroupListItemDto, new();
    }
}