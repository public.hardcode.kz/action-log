using System.Collections.Generic;
using System.Threading.Tasks;
using ActionLog.Module.Models;
using ActionLog.Module.Repos;
using ActionLog.Module.Utils;
using Microsoft.EntityFrameworkCore;

namespace ActionLog.Module.Logics.Impl
{
    public class ActionLogSettingLogic<TContext> : IActionLogSettingLogic
    where TContext : DbContext
    {
        private readonly IActionLogTrackedClassRepo<TContext> _actionLogTrackedClassRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ActionLogSettingLogic{TContext}"/> class.
        /// </summary>
        /// <param name="actionLogTrackedClassRepo">IActionLogTrackedClassRepo.</param>
        public ActionLogSettingLogic(IActionLogTrackedClassRepo<TContext> actionLogTrackedClassRepo)
        {
            _actionLogTrackedClassRepo = actionLogTrackedClassRepo;
        }

        /// <inheritdoc/>
        public async Task TrackedClass<T>(bool isTracked)
        {
            var classInfo = ActionLogUtil.GetActionLogTracked<T>();
            var existClass = await _actionLogTrackedClassRepo
                .SelectQuery(x => x.ClassFullName == classInfo.ClassFullName)
                .FirstOrDefaultAsync();
            var isNew = false;
            if (existClass == null)
            {
                existClass = new ActionLogTrackedClass();
                isNew = true;
            }

            existClass.ClassName = classInfo.ClassName;
            existClass.ClassFullName = classInfo.ClassFullName;
            existClass.IsTracked = isTracked;
            existClass.AssemblyQualifiedName = classInfo.AssemblyQualifiedName;

            if (isNew)
            {
                await _actionLogTrackedClassRepo.AddAsync(existClass);
                return;
            }

            await _actionLogTrackedClassRepo.UpdateAsync(existClass);
        }

        /// <inheritdoc/>
        public Task<IEnumerable<ActionLogTrackedClass>> Get()
        {
            return _actionLogTrackedClassRepo.Get();
        }
    }
}