using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using ActionLog.Module.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace ActionLog.Module.Logics.Impl
{
    public class ActionLogBaseLogic<TContext> : IActionLogBaseLogic
        where TContext : DbContext

    {
        private readonly TContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="ActionLogBaseLogic{TContext}"/> class.
        /// </summary>
        /// <param name="context">DbContext.</param>
        /// <param name="httpContextAccessor">httpContextAccessor.</param>
        public ActionLogBaseLogic(TContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public void DetectContextChanges(EntityEntry change, Guid groupGuid)
        {
            switch (change.State)
            {
                case EntityState.Modified:
                    Modified(change, groupGuid);
                    break;
                case EntityState.Deleted:
                    Modified(change, groupGuid);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void NewObject(EntityEntry change, Guid groupGuid)
        {
            var type = change.Entity.GetType();
            var primaryKeyValue = GetPrimaryKeyValue(change.Entity);

            var changeLogged = new Models.ActionLog
            {
                ObjectName = type.Name,
                ObjectFullName = type.FullName,
                ObjectId = primaryKeyValue,
                DateCreate = DateTime.UtcNow,
                UserId = GetCurrentUserId(),
                State = EntityState.Added,
                GroupGuid = groupGuid,
                ActionLogTypeEnum = GetActionLogTypeEnum(),
                ActionLogTypeGroupId = GetActionLogTypeGuid(),
            };

            _context.Add(changeLogged);
        }

        public void SaveExtraData()
        {
            var value = GetActionLogExtraData();
            if (string.IsNullOrEmpty(value)) return;
            
            var groupId = GetActionLogTypeGuid();
            var guids = GetExtraDataGuids();
            if (guids.Contains(groupId)) return;

            var entity = new ActionLogExtraData
            {
                GroupGuid = groupId,
                Value = value,
                DateCreate = DateTime.Now
            };
            UpdateExtraDataGuids(groupId);
            _context.Add(entity);
        }

        private void Modified(EntityEntry change, Guid groupGuid)
        {
            var type = change.Entity.GetType();
            var primaryKeyValue = GetPrimaryKeyValue(change.Entity);
            var now = DateTime.UtcNow;
            var changeLoggeds = new List<Models.ActionLog>();

            foreach (var prop in change.Properties)
            {
                var hasForeignKey = prop.Metadata.PropertyInfo.CustomAttributes.Any(attribute =>
                    attribute.AttributeType.Name == "ForeignKeyAttribute");

                if (hasForeignKey || prop.Metadata.PropertyInfo.GetGetMethod().IsVirtual)
                {
                    continue;
                }

                if (change.State == EntityState.Modified)
                {
                    if (prop.OriginalValue?.ToString() == prop.CurrentValue?.ToString())
                    {
                        continue;
                    }
                }

                var changeLogged = new Models.ActionLog
                {
                    Column = prop.Metadata.Name,
                    ObjectName = type.Name,
                    ObjectId = primaryKeyValue,
                    DateCreate = now,
                    OldValue = prop.OriginalValue?.ToString(),
                    NewValue = prop.CurrentValue?.ToString(),
                    UserId = GetCurrentUserId(),
                    ObjectFullName = type.FullName,
                    State = change.State,
                    GroupGuid = groupGuid,
                    ActionLogTypeEnum = GetActionLogTypeEnum(),
                    ActionLogTypeGroupId = GetActionLogTypeGuid(),
                };
                changeLoggeds.Add(changeLogged);
            }

            _context.AddRangeAsync(changeLoggeds).Wait();
        }

        protected virtual string GetPrimaryKeyValue<T>(T entity)
        {
            var test = entity;
            var test2 = test.GetType();
            var keyName = _context.Model.FindEntityType(test2).FindPrimaryKey().Properties
                .Select(x => x.Name).Single();
            return entity.GetType().GetProperty(keyName)?.GetValue(entity, null)?.ToString();
        }

        private string GetCurrentUserId()
        {
            object userId = null;
            _httpContextAccessor?.HttpContext?.Items.TryGetValue("actionLogUserId", out userId);
            userId ??= _httpContextAccessor?.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == ClaimTypes.Sid || c.Type == "sid")?.Value;
            return userId?.ToString();
        }
        
        private List<Guid> GetExtraDataGuids()
        {
            object guids = null;
            _httpContextAccessor?.HttpContext?.Items.TryGetValue("actionLogExtraGuids", out guids);
            return (List<Guid>) guids ?? new List<Guid>();
        }
        
        private void UpdateExtraDataGuids(Guid guid)
        {
            var guids = GetExtraDataGuids();
            guids.Add(guid);
            _httpContextAccessor.HttpContext.Items["actionLogExtraGuids"] = guids;
        }

        private string GetActionLogTypeEnum()
        {
                object type = null;
                _httpContextAccessor?.HttpContext?.Items.TryGetValue("actionLogTypeEnum", out type);
                return (string)type ?? "Default";
        }
        
        private Guid GetActionLogTypeGuid()
        {
            object value = null;
            _httpContextAccessor?.HttpContext?.Items.TryGetValue("actionLogTypeGuid", out value);
            return (Guid?)value ?? Guid.NewGuid();
        }
        
        private string GetActionLogExtraData()
        {
            object value = null;
            _httpContextAccessor?.HttpContext?.Items.TryGetValue("actionLogExtraData", out value);
            return (string)value;
        }
    }
}