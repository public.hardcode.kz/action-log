using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace ActionLog.Module.Logics.Impl
{
    public class ActionLogHttpContextLogic : IActionLogHttpContextLogic, IActionLogExtraDataLogic
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ActionLogHttpContextLogic(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _httpContextAccessor.HttpContext ??= new DefaultHttpContext();
        }

        public void SetActionLogType(string type)
        {
            _httpContextAccessor.HttpContext.Items["actionLogTypeEnum"] = type;
        }

        public string GetActionLogType()
        {
            object type = null;
            _httpContextAccessor?.HttpContext?.Items.TryGetValue("actionLogTypeEnum", out type);
            return (string)type ?? "Default";
        }

        public void SetActionLogCurrentUserId(string userId)
        {
            _httpContextAccessor.HttpContext.Items["actionLogUserId"] = userId;
        }

        public string GetActionLogCurrentUserId()
        {
            object userId = null;
            _httpContextAccessor?.HttpContext?.Items.TryGetValue("actionLogUserId", out userId);
            userId ??= _httpContextAccessor?.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == ClaimTypes.Sid || c.Type == "sid")?.Value;
            return userId?.ToString();
        }

        public void SetExtraData(object data)
        {
            var json = JsonConvert.SerializeObject(data);
            _httpContextAccessor.HttpContext.Items["actionLogExtraData"] = json;
        }

        public T GetExtraData<T>() where T : class
        {
            try
            {
                object res = null;
                _httpContextAccessor?.HttpContext?.Items.TryGetValue("actionLogExtraData", out res);
                return res == null ? default : JsonConvert.DeserializeObject<T>((string)res);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
    }
}