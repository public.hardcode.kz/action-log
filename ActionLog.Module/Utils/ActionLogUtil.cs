using System;
using System.Linq;
using ActionLog.Module.LogAttributes;
using ActionLog.Module.Models;

namespace ActionLog.Module.Utils
{
    public static class ActionLogUtil
    {
        /// <summary>
        /// Получение ActionLogTrackedClass из класса
        /// </summary>
        /// <typeparam name="T">Class.</typeparam>
        /// <returns>ActionLogTrackedClass.</returns>
        public static ActionLogTrackedClass GetActionLogTracked<T>()
        {
            var type = typeof(T);
            return new ActionLogTrackedClass
            {
                ClassName = type.Name,
                IsTracked = true,
                AssemblyQualifiedName = type.AssemblyQualifiedName,
                ClassFullName = type.FullName,
            };
        }
        
        public static Type GetExtraDataType(this Enum genericEnum)
        {
            var genericEnumType = genericEnum.GetType();
            var memberInfo = genericEnumType.GetMember(genericEnum.ToString());
            if (memberInfo.Length > 0)
            {
                var attribs = memberInfo[0].GetCustomAttributes(typeof(AppActionLogExtraTypeAttribute), false);
                if (attribs.Any())
                {
                    return ((AppActionLogExtraTypeAttribute)attribs.ElementAt(0)).GetExtraType();
                }
            }

            return null;
        }
    }
}