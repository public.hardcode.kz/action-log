using System;
using System.Linq.Expressions;
using ActionLog.Module.Logics;

namespace ActionLog.Module.LogAttributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ActionLogResolverAttribute : Attribute
    {
        private readonly Type _resolver;

        public ActionLogResolverAttribute(Type resolver)
        {
            _resolver = resolver;
        }

        public Type GetResolver()
        {
            return _resolver;
        }
    }
}