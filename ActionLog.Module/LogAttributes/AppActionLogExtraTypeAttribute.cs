using System;

namespace ActionLog.Module.LogAttributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class AppActionLogExtraTypeAttribute : Attribute
    {
        private readonly Type _type;

        public AppActionLogExtraTypeAttribute(Type type)
        {
            _type = type;
        }
        
        public Type GetExtraType()
        {
            return _type;
        }
    }
}