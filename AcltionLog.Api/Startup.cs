using System;
using AcltionLog.Api.ActionLogResolver;
using AcltionLog.Api.Data;
using AcltionLog.Api.Data.Models;
using AcltionLog.Api.Dto;
using ActionLog.Module;
using ActionLog.Module.Logics;
using HardCode.ICacheService;
using HardCode.Services.Cache;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AcltionLog.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            string connectionStr = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApplicationDbContext>(options => options.UseNpgsql(connectionStr));

            var useRedis = Convert.ToBoolean(Configuration.GetValue("Redis:UseRedis", false));
            services.ConfigureHardCodeCache(
                Convert.ToBoolean(useRedis),
                Configuration.GetValue("Redis:ConnectionString", ""),
                Configuration.GetValue("Redis:Name", "actionLogTest")
            );
            


            services.ConfigureActionLog<ApplicationDbContext, User, string, ActionGroupLogExDto, ICacheService>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTransient<IActionLogResolver, ActionLogRoleIdResolver>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}