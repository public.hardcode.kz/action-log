

using System.Collections.Generic;
using AcltionLog.Api.Data.Models;
using ActionLog.Module.Context;
using ActionLog.Module.Logics.Impl;
using ActionLog.Module.Models;
using HardCode.ICacheService;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace AcltionLog.Api.Data
{
    /// <summary>
    /// Db context
    /// </summary>
    public class ApplicationDbContext : ActionLogDbContext<ApplicationDbContext>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICacheService _cacheService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationDbContext"/> class.
        /// </summary>
        /// <param name="options">DbContextOptions.</param>
        /// <param name="httpContextAccessor">IHttpContextAccessor.</param>
        /// <param name="cacheService">ICacheService.</param>
        public ApplicationDbContext(
            DbContextOptions options,
            IHttpContextAccessor httpContextAccessor,
            ICacheService cacheService)
            : base(options)
        {
            _httpContextAccessor = httpContextAccessor;
            _cacheService = cacheService;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationDbContext"/> class.
        /// </summary>
        /// <param name="options">DbContextOptions.</param>
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationDbContext"/> class.
        /// </summary>
        /// <param name="connectionString">Connection string.</param>
        public ApplicationDbContext(string connectionString)
            : base(GetOptions(connectionString))
        {
        }

        private static DbContextOptions GetOptions(string connectionString)
        {
            return NpgsqlDbContextOptionsExtensions.UseNpgsql(new DbContextOptionsBuilder(), connectionString).Options;
        }

        public DbSet<User> AuthUsers { get; set; }

        public DbSet<Role> Roles { get; set; }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ActionLog.Module.Models.ActionLog>()
                .HasIndex(b => b.ObjectId);
        }
        
        /// <inheritdoc/>
        public override ActionLogBaseLogic<ApplicationDbContext> CreateActionLogBaseLogic()
        {
            return new ActionLogBaseLogic<ApplicationDbContext>(this, _httpContextAccessor);
        }

        public override List<ActionLogTrackedClass> GetTrackedEntities()
        {
            if (_cacheService == null) return base.GetTrackedEntities();
            var ret = _cacheService.Get<List<ActionLogTrackedClass>>("TrackedEntities");
            if (ret != null) return ret;
            ret = base.GetTrackedEntities();
            _cacheService.Store("TrackedEntities", ret, 5);
            return ret;
        }
    }
}