using System.ComponentModel;
using AcltionLog.Api.Dto.ActionLogExtraDataDtos;
using ActionLog.Module.LogAttributes;

namespace AcltionLog.Api.Primitives
{
    public enum ActionLogTypeEnum
    {
        [Description("Тип по умолчанию")]
        Default,

        [AppActionLogExtraType(typeof(ActionLogUserUpdateExtraDto))]
        [Description("Обновление пользователя")]
        UpdateUser,

        [AppActionLogExtraType(typeof(ActionLogUserCreateExtraDto))]
        [Description("Создание пользователя")]
        CreateUser,
    }
}