using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AcltionLog.Api.Data;
using AcltionLog.Api.Data.Models;
using AcltionLog.Api.Dto;
using AcltionLog.Api.Dto.ActionLogExtraDataDtos;
using AcltionLog.Api.Primitives;
using ActionLog.Module.Dtos;
using ActionLog.Module.Dtos.Grouped;
using ActionLog.Module.LogAttributes;
using ActionLog.Module.Logics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AcltionLog.Api.Controllers
{
    /// <summary>
    /// ActionUser controller
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IActionLogSettingLogic _actionLogSettingLogic;
        private readonly IActionLogViewLogic<ActionGroupLogExDto, User, string> _actionLogViewLogic;
        private readonly ApplicationDbContext _dbContext;
        private readonly IActionLogHttpContextLogic _actionLogHttpContextLogic;
        private readonly IActionLogExtraDataLogic _actionLogExtraDataLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserController"/> class.
        /// </summary>
        public UserController(
            IActionLogSettingLogic actionLogSettingLogic,
            ApplicationDbContext dbContext,
            IActionLogViewLogic<ActionGroupLogExDto, User, string> actionLogViewLogic,
            IActionLogHttpContextLogic actionLogHttpContextLogic, 
            IActionLogExtraDataLogic actionLogExtraDataLogic)
        {
            _actionLogSettingLogic = actionLogSettingLogic;
            _dbContext = dbContext;
            _actionLogViewLogic = actionLogViewLogic ?? throw new ArgumentNullException(nameof(actionLogViewLogic));
            _actionLogHttpContextLogic = actionLogHttpContextLogic;
            _actionLogExtraDataLogic = actionLogExtraDataLogic;
        }

        
        [HttpGet]
        public void TestSetActionLogTypeEnum()
        {
            _actionLogHttpContextLogic.SetActionLogType("test");
            var test = _actionLogHttpContextLogic.GetActionLogType();
        }
        
      
        [HttpGet]
        public void TestSetActionLogCurrentUserId()
        {
            _actionLogHttpContextLogic.SetActionLogCurrentUserId("1");
            var test = _actionLogHttpContextLogic.GetActionLogCurrentUserId();
        }
        
      
        [HttpGet]
        public void TestSetExtraData()
        {
            _actionLogHttpContextLogic.SetExtraData(new ActionLogClassAttributeInfoDto
            {
                ColumnName = "Test"
            });
            
            var test = _actionLogHttpContextLogic.GetExtraData<ActionLogClassAttributeInfoDto>();
        }
      
        [HttpGet]
        public void TestSetExtraData3()
        {
            var test = _actionLogHttpContextLogic.GetExtraData<ActionLogClassAttributeInfoDto>();
        }
        
        [HttpGet]
        public void TestSetExtraData2()
        {
            _actionLogExtraDataLogic.SetExtraData(new
            {
                Test = "test"
            });
        }
        
        /// <summary>
        /// Test method
        /// </summary>
        /// <returns>string.</returns>
        [HttpGet]
        public string Test()
        {
            return "test";
        }

        /// <summary>
        /// Bla
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the result of the asynchronous operation.</returns>
        [AppActionLogType(ActionLogTypeEnum.CreateUser)]
        [HttpGet]
        public async Task CreateNewUser()
        {
            var user = new User
            {
                Email = "nurdinkz@gmail.com",
                Fio = "Nurdin",
                UserName = "nurda",
                RoleId = 1,
            };
            

            await _dbContext.AddAsync(user);
            await _dbContext.SaveChangesAsync();
        }



        /// <summary>
        /// Update role
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the result of the asynchronous operation.</returns>
        [HttpGet]
        public async Task UpdateRole()
        {
            var role = await _dbContext.Roles.FirstOrDefaultAsync();
            role.Name = DateTime.Now.ToString();

            _actionLogHttpContextLogic.SetExtraData(new ActionLogUserUpdateExtraDto()
            {
                Title = "Обновление пользователя",
                DateTime = DateTime.Now,
                UserId = role.Id.ToString(),
            });
            await _dbContext.SaveChangesAsync();
        }


        [HttpGet("{id}")]
        public async Task<ActionLogGroupTypeDto> Grouped(Guid id)
        {
            var items = await _actionLogViewLogic.GetGrouped(id, ActionLogUserExpression());
            var items1 = await _actionLogViewLogic.GetGrouped(id);
            var items2 = await _actionLogViewLogic.GetGrouped<ActionLogGroupTypeExDto>(id, ActionLogUserExpression());
            var items3 = await _actionLogViewLogic.GetGrouped<ActionLogGroupTypeExDto>(id);
            return items;
        }

        [HttpGet]
        public async Task InsertRange()
        {
            var sw = new Stopwatch();
            sw.Start();

            var addList = new List<Role>();

            for (int i = 1; i < 10000; i++)
            {
                var s = i.ToString();
                addList.Add(new Role()
                {
                    Name = s,
                    Id = i
                });
            }

            await _dbContext.Roles.AddRangeAsync(addList);
            await _dbContext.SaveChangesAsync();

            sw.Stop();
            Console.WriteLine($"{sw.ElapsedMilliseconds} мс");
        }

        
        [HttpGet]
        public async Task InsertRangeUser()
        {
            var sw = new Stopwatch();
            sw.Start();

            var addList = new List<User>();

            for (int i = 1; i < 10000; i++)
            {
                var s = i.ToString();
                addList.Add(new User()
                {
                    Email = s,
                    Fio = s,
                    UserName = s,
                });
            }

            await _dbContext.AuthUsers.AddRangeAsync(addList);
            await _dbContext.SaveChangesAsync();

            sw.Stop();
            Console.WriteLine($"{sw.ElapsedMilliseconds} мс");
        }
        
        /// <summary>
        /// Update user
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the result of the asynchronous operation.</returns>
        [HttpGet]
        [AppActionLogType(ActionLogTypeEnum.UpdateUser)]
        public async Task UpdateUser()
        {
            var user = await _dbContext.AuthUsers.FirstOrDefaultAsync();
   

            user.RoleId = 999;
            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Get log by userId
        /// </summary>
        /// <param name="userId">User ID.</param>
        /// <returns>A <see cref="Task{TResult}"/> representing the result of the asynchronous operation.</returns>
        [HttpGet("{userId}")]
        public async Task<object> GetInfo(string userId)
        {
            var query = new ActionLogQuery
            {
                Page = 1,
                RowCount = 100,
                ObjectFullName = typeof(User).FullName,
                ObjectId = userId,
                ActionLogTypes = new List<string>
                {
                    ActionLogTypeEnum.CreateUser.ToString(),
                    ActionLogTypeEnum.UpdateUser.ToString(),
                },
            };

            var items = await _actionLogViewLogic.Get(query, ActionLogUserExpression());

            return items;
        }

        [NonAction]
        private Expression<Func<User, ActionLogUserDto<string>>> ActionLogUserExpression()
        {
            return x => new ActionLogUserDto<string>
            {
                Email = x.Email,
                Id = x.Id,
                UserName = x.Fio,
            };
        }

        /// <summary>
        /// Add new user in tracked class
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the result of the asynchronous operation.</returns>
        [HttpGet]
        public async Task AddUserTable()
        {
            await _actionLogSettingLogic.TrackedClass<User>(true);
            await _actionLogSettingLogic.TrackedClass<Role>(true);
        }
    }
}