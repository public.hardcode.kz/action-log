using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AcltionLog.Api.Data.Models;
using AcltionLog.Api.Dto;
using ActionLog.Module.Dtos;
using ActionLog.Module.Dtos.Grouped;
using ActionLog.Module.Logics;
using Microsoft.AspNetCore.Mvc;

namespace AcltionLog.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ActionLogGroupedListController : Controller
    {
        private readonly IActionLogViewLogic<ActionGroupLogExDto, User, string> _actionLogViewLogic;

        public ActionLogGroupedListController(IActionLogViewLogic<ActionGroupLogExDto, User, string> actionLogViewLogic)
        {
            _actionLogViewLogic = actionLogViewLogic;
        }

        /// <summary>
        /// Test method
        /// </summary>
        /// <returns>string.</returns>
        [HttpGet]
        public string Test()
        {
            return "test";
        }


        [HttpGet]
        public async Task<ActionLogSearhResultDto<ActionLogGroupListItemExtraDto>> GroupList([FromQuery]ActionLogGroupListQueryDto filter)
        {
            var data = await _actionLogViewLogic.GroupList<ActionLogGroupListItemExtraDto>(filter, ActionLogUserExpression());
            return data;
        }

        [NonAction]
        private Expression<Func<User, ActionLogUserDto<string>>> ActionLogUserExpression()
        {
            return x => new ActionLogUserDto<string>
            {
                Email = x.Email,
                Id = x.Id,
                UserName = x.Fio,
            };
        }

    }
}