using System;
using AcltionLog.Api.Primitives;
using AcltionLog.Api.Utils;
using ActionLog.Module.Dtos.Grouped;

namespace AcltionLog.Api.Dto
{
    public class ActionLogGroupListItemExtraDto : ActionLogGroupListItemDto
    {
        private ActionLogTypeEnum ActionLogType => Enum.Parse<ActionLogTypeEnum>(Action);

        public string ActionTitle
        {
            get
            {
                if (string.IsNullOrEmpty(Action))
                {
                    return string.Empty;
                }

                return ActionLogType.GetDescription();
            }
        }

    }
}