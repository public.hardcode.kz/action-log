using System.Collections.Generic;

namespace AcltionLog.Api.Dto.ActionLogExtraDataDtos
{
    public class ActionLogUserCreateExtraDto
    {
        public string Title { get; set; }

        public int ExtraInt { get; set; }

        public List<string> Array { get; set; }
    }
}