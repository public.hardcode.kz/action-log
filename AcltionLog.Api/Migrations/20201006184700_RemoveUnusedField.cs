﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AcltionLog.Api.Migrations
{
    public partial class RemoveUnusedField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClassNamespace",
                table: "ActionLogTrackeds");

            migrationBuilder.DropColumn(
                name: "DateUpdate",
                table: "ActionLogs");

            migrationBuilder.DropColumn(
                name: "ObjectNamespace",
                table: "ActionLogs");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ClassNamespace",
                table: "ActionLogTrackeds",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateUpdate",
                table: "ActionLogs",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ObjectNamespace",
                table: "ActionLogs",
                type: "text",
                nullable: true);
        }
    }
}
