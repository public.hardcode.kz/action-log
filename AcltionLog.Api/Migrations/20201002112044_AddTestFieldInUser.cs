﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AcltionLog.Api.Migrations
{
    public partial class AddTestFieldInUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Data",
                table: "AuthUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Data",
                table: "AuthUsers");
        }
    }
}
