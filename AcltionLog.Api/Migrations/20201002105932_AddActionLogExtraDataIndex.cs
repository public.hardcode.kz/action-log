﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AcltionLog.Api.Migrations
{
    public partial class AddActionLogExtraDataIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ActionLogExtraDatas_GroupGuid",
                table: "ActionLogExtraDatas",
                column: "GroupGuid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ActionLogExtraDatas_GroupGuid",
                table: "ActionLogExtraDatas");
        }
    }
}
